"use strict";
var Post = (function () {
    function Post(id, title, content, readable_content, slug, published, created, updated) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.readable_content = readable_content;
        this.slug = slug;
        this.published = published || false;
        this.created = created || '';
        this.updated = updated || '';
        this.updateUrl = '/dashboard/update/' + id;
    }
    return Post;
}());
exports.Post = Post;
//# sourceMappingURL=post.js.map