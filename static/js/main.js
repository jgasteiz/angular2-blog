"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var blog_component_1 = require('./components/blog/blog.component');
var blog_routes_1 = require('./components/blog/blog.routes');
platform_browser_dynamic_1.bootstrap(blog_component_1.BlogComponent, [blog_routes_1.APP_ROUTER_PROVIDERS]);
//# sourceMappingURL=main.js.map