"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var forms_1 = require('@angular/forms');
var dashboard_component_1 = require('./components/dashboard/dashboard.component');
var dashboard_routes_1 = require('./components/dashboard/dashboard.routes');
platform_browser_dynamic_1.bootstrap(dashboard_component_1.DashboardComponent, [
    dashboard_routes_1.APP_ROUTER_PROVIDERS,
    forms_1.disableDeprecatedForms(),
    forms_1.provideForms()
]);
//# sourceMappingURL=dashboard.js.map