"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var post_1 = require('../models/post');
require('rxjs/add/operator/map');
var DataService = (function () {
    function DataService(http) {
        this.http = http;
        this.POSTS_ENDPOINT = '/api/blogposts/';
        this.POSTS_DASHBOARD_ENDPOINT = '/dashboard/api/blogposts/';
        this.posts = [];
    }
    /**
     * Fetch the posts from the api.
     * @return {[type]} [description]
     */
    DataService.prototype.fetchPostsFromAPI = function () {
        var _this = this;
        this.posts = [];
        this.http.get(this.POSTS_ENDPOINT + '?format=json')
            .map(function (response) { return response.json(); })
            .subscribe(function (posts) {
            posts.forEach(function (post) {
                this.posts.push(new post_1.Post(post.id, post.title, post.content, post.readable_content, post.slug));
            }, _this);
        });
    };
    /**
     * Fetch the posts from the api.
     * @return {[type]} [description]
     */
    DataService.prototype.fetchPostsFromDashboardAPI = function () {
        var _this = this;
        this.posts = [];
        this.http.get(this.POSTS_DASHBOARD_ENDPOINT + '?format=json')
            .map(function (response) { return response.json(); })
            .subscribe(function (posts) {
            posts.forEach(function (post) {
                this.posts.push(new post_1.Post(post.id, post.title, post.content, post.readable_content, post.slug, post.published, post.created, post.updated));
            }, _this);
        });
    };
    /**
     * Fetch a post from the api.
     * @param  {string} slug [description]
     * @return {[type]}    [description]
     */
    DataService.prototype.fetchPostFromAPI = function (slug) {
        return this.http.get(this.POSTS_ENDPOINT + slug + '?format=json')
            .map(function (response) { return response.json(); });
    };
    DataService.prototype.fetchPostFromDashboardAPI = function (id) {
        return this.http.get(this.POSTS_DASHBOARD_ENDPOINT + id + '?format=json')
            .map(function (response) { return response.json(); });
    };
    /**
     * Create a post with the given properties.
     * @param post
     * @param onComplete
     */
    DataService.prototype.createPost = function (post, onComplete) {
        var _this = this;
        return this.http.post('/dashboard/api/blogposts/', {
            title: post.title,
            content: post.content,
            slug: post.slug,
            published: post.published
        }, this._getRequestOptions())
            .map(function (response) { return response.json(); })
            .subscribe(function (data) {
            _this.posts.push(new post_1.Post(data.id, data.title, data.content, data.readable_content, data.slug, data.published, data.created, data.updated));
        }, function (err) { return console.error(err); }, onComplete());
    };
    ;
    /**
     * Update a given post.
     * @param post
     * @param onComplete
     */
    DataService.prototype.updatePost = function (post, onComplete) {
        var _this = this;
        this.http.put('/dashboard/api/blogposts/' + post.id + '/', {
            id: post.id,
            title: post.title,
            content: post.content,
            slug: post.slug,
            published: post.published
        }, this._getRequestOptions())
            .map(function (response) { return response.json(); })
            .subscribe(function (data) {
            var postIndex = _this.posts
                .map(function (post) { return post.id; })
                .indexOf(post.id);
            _this.posts[postIndex] = new post_1.Post(data.id, data.title, data.content, data.readable_content, data.slug, data.published, data.created, data.updated);
        }, function (err) { return console.error(err); }, onComplete());
    };
    ;
    /**
     * Delete a given post.
     * @param post
     */
    DataService.prototype.deletePost = function (post) {
        var _this = this;
        if (!confirm('Are you sure you want to delete the post `' + post.title + '`?')) {
            return false;
        }
        this.http.delete('/dashboard/api/blogposts/' + post.id + '/', this._getRequestOptions())
            .subscribe(function (data) {
            var postIndex = _this.posts
                .map(function (post) { return post.id; })
                .indexOf(post.id);
            _this.posts.splice(postIndex, 1);
        });
    };
    ;
    /**
     * Return the request options for PUT, POST and DELETE requests.
     * @returns {CustomRequestOptions}
     * @private
     */
    DataService.prototype._getRequestOptions = function () {
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        return new CustomRequestOptions({ headers: headers });
    };
    ;
    DataService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
var CustomRequestOptions = (function (_super) {
    __extends(CustomRequestOptions, _super);
    function CustomRequestOptions(options) {
        _super.call(this);
        if (options) {
            this.headers = options['headers'];
        }
        this.headers.append('X-CSRFToken', this.getCookie('csrftoken'));
    }
    CustomRequestOptions.prototype.getCookie = function (name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length === 2) {
            return parts.pop().split(";").shift();
        }
    };
    return CustomRequestOptions;
}(http_1.BaseRequestOptions));
//# sourceMappingURL=data.service.js.map