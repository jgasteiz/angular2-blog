"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var post_1 = require('../../models/post');
var data_service_1 = require('../../services/data.service');
var DashboardPostFormComponent = (function () {
    function DashboardPostFormComponent(route, router, dataService, fb) {
        this.route = route;
        this.router = router;
        this.dataService = dataService;
        this.fb = fb;
    }
    DashboardPostFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.post = new post_1.Post(-1, '', '', '', '', false);
        this.postForm = this.fb.group({
            'title': [this.post.title, forms_1.Validators.required],
            'slug': [this.post.slug, forms_1.Validators.required],
            'content': [this.post.content, forms_1.Validators.required],
            'published': [this.post.published],
        });
        this.submitted = false;
        this.route.params.subscribe(function (params) {
            if (params.id) {
                _this.dataService.fetchPostFromDashboardAPI(params.id)
                    .subscribe(function (post) {
                    _this.post = new post_1.Post(post.id, post.title, post.content, post.readable_content, post.slug, post.published, post.created, post.updated);
                });
            }
        });
    };
    DashboardPostFormComponent.prototype.onSubmit = function (form) {
        var _this = this;
        this.submitted = true;
        // Set the slug if it hasn't been set.
        if (this.post.slug === '') {
            this.post.slug = this.slugify(form.title);
        }
        else {
            this.post.slug = this.slugify(this.post.slug);
        }
        // Prevent submission if there are errors in the title or in the content.
        if (!this.postForm.find('title').valid || !this.postForm.find('content').valid) {
            return;
        }
        // Update a post
        if (this.post.id !== -1) {
            this.dataService.updatePost(this.post, function () {
                _this.router.navigateByUrl('/dashboard');
            });
        }
        else {
            this.dataService.createPost(this.post, function () {
                _this.router.navigateByUrl('/dashboard');
            });
        }
    };
    DashboardPostFormComponent.prototype.slugify = function (text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text
    };
    DashboardPostFormComponent = __decorate([
        core_1.Component({
            directives: [router_1.ROUTER_DIRECTIVES, forms_1.REACTIVE_FORM_DIRECTIVES],
            selector: 'dashboard-post-form',
            styleUrls: ['static/app/components/dashboard-postform/dashboard-postform.component.css'],
            templateUrl: 'static/app/components/dashboard-postform/dashboard-postform.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, data_service_1.DataService, forms_1.FormBuilder])
    ], DashboardPostFormComponent);
    return DashboardPostFormComponent;
}());
exports.DashboardPostFormComponent = DashboardPostFormComponent;
//# sourceMappingURL=dashboard-postform.component.js.map