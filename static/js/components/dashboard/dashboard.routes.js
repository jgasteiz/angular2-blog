"use strict";
var router_1 = require('@angular/router');
var dashboard_postlist_component_1 = require('../dashboard-postlist/dashboard-postlist.component');
var dashboard_postform_component_1 = require('../dashboard-postform/dashboard-postform.component');
exports.routes = [
    { path: 'dashboard', component: dashboard_postlist_component_1.DashboardPostListComponent },
    { path: 'dashboard/new-post', component: dashboard_postform_component_1.DashboardPostFormComponent },
    { path: 'dashboard/update/:id', component: dashboard_postform_component_1.DashboardPostFormComponent },
];
exports.APP_ROUTER_PROVIDERS = [router_1.provideRouter(exports.routes)];
//# sourceMappingURL=dashboard.routes.js.map