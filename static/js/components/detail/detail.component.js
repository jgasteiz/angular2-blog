"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var post_1 = require('../../models/post');
var post_component_1 = require('../post/post.component');
var data_service_1 = require('../../services/data.service');
var DetailComponent = (function () {
    function DetailComponent(route, router, dataService) {
        this.route = route;
        this.router = router;
        this.dataService = dataService;
    }
    DetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.post = null;
        this.route.params.subscribe(function (params) {
            _this.dataService.fetchPostFromAPI(params['id'])
                .subscribe(function (post) {
                _this.post = new post_1.Post(post.id, post.title, post.content, post.readable_content, post.slug);
            });
        });
    };
    DetailComponent = __decorate([
        core_1.Component({
            directives: [post_component_1.PostComponent],
            selector: 'detail',
            templateUrl: 'static/app/components/detail/detail.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, data_service_1.DataService])
    ], DetailComponent);
    return DetailComponent;
}());
exports.DetailComponent = DetailComponent;
//# sourceMappingURL=detail.component.js.map