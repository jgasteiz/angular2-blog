"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var article_1 = require('../models/article');
var comment_1 = require('../models/comment');
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
var DataService = (function () {
    function DataService(http) {
        this.http = http;
        this.articles = [];
    }
    /**
     * Fetch the articles from the api.
     * @return {[type]} [description]
     */
    DataService.prototype.fetchArticlesFromAPI = function () {
        var _this = this;
        this.articles = [];
        this.http.get('https://node-hnapi-javiman.herokuapp.com/news')
            .map(function (response) { return response.json(); })
            .subscribe(function (articles) {
            articles.forEach(function (article) {
                this.articles.push(new article_1.Article(article.id, article.title, article.url, article.points, article.user, article.comments_count));
            }, _this);
        });
    };
    /**
     * Fetch the comments from the hn api.
     * @param  {number} itemId [description]
     * @return {[type]}        [description]
     */
    DataService.prototype.fetchCommentsFromAPI = function (itemId) {
        return this.http.get('https://node-hnapi-javiman.herokuapp.com/item/' + itemId)
            .map(function (response) { return response.json(); });
    };
    /**
     * Given an article or a comment, return its comments if it has any.
     * @param  {any}       item [description]
     * @return {Comment[]}      [description]
     */
    DataService.prototype.getCommentsFromItem = function (item) {
        var comments = [];
        if (item.comments && item.comments.length > 0) {
            item.comments.forEach(function (comment) {
                comments.push(new comment_1.Comment(comment.id, comment.user, comment.content, this.getCommentsFromItem(comment)));
            }, this);
        }
        return comments;
    };
    DataService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
//# sourceMappingURL=data.service.js.map