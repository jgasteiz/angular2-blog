"use strict";
var router_1 = require('@angular/router');
var postlist_component_1 = require('../postlist/postlist.component');
var about_component_1 = require('../about/about.component');
var detail_component_1 = require('../detail/detail.component');
exports.routes = [
    { path: '', component: postlist_component_1.PostListComponent },
    { path: 'about', component: about_component_1.AboutComponent },
    { path: ':id', component: detail_component_1.DetailComponent },
];
exports.APP_ROUTER_PROVIDERS = [router_1.provideRouter(exports.routes)];
//# sourceMappingURL=blog.routes.js.map