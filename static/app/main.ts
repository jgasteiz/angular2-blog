import { bootstrap }    from '@angular/platform-browser-dynamic';
import { BlogComponent } from './components/blog/blog.component';
import { APP_ROUTER_PROVIDERS } from './components/blog/blog.routes';

bootstrap(BlogComponent, [APP_ROUTER_PROVIDERS]);
