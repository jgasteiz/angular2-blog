import { Component } from '@angular/core';
import { PostComponent } from '../post/post.component';
import { DataService } from '../../services/data.service';

@Component({
    directives: [PostComponent],
    selector: 'post-list',
    styleUrls: ['static/app/components/postlist/postlist.component.css'],
    templateUrl: 'static/app/components/postlist/postlist.component.html'
})
export class PostListComponent {
    constructor (private dataService: DataService) {}

    getPosts() {
        return this.dataService.posts;
    }
}
