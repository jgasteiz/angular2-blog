import { Component } from '@angular/core';
import {
    FORM_DIRECTIVES,
    REACTIVE_FORM_DIRECTIVES,
    FormBuilder,
    FormGroup,
    Validators
} from '@angular/forms';
import { ActivatedRoute, Router, ROUTER_DIRECTIVES } from '@angular/router';
import { Post } from '../../models/post';
import { DataService } from '../../services/data.service';

@Component({
    directives: [ROUTER_DIRECTIVES, REACTIVE_FORM_DIRECTIVES],
    selector: 'dashboard-post-form',
    styleUrls: ['static/app/components/dashboard-postform/dashboard-postform.component.css'],
    templateUrl: 'static/app/components/dashboard-postform/dashboard-postform.component.html'
})
export class DashboardPostFormComponent {
    post: Post;
    postForm: FormGroup;

    submitted: boolean;

    constructor (
        private route: ActivatedRoute,
        private router: Router,
        private dataService: DataService,
        private fb: FormBuilder
    ) {}

    ngOnInit () {
        this.post = new Post(-1, '', '', '', '', false);

        this.postForm = this.fb.group({
            'title': [this.post.title, Validators.required],
            'slug': [this.post.slug, Validators.required],
            'content': [this.post.content, Validators.required],
            'published': [this.post.published],
        });

        this.submitted = false;

        this.route.params.subscribe((params: any) => {
            if (params.id) {
                this.dataService.fetchPostFromDashboardAPI(params.id)
                    .subscribe((post: any) => {
                        this.post = new Post(
                            post.id,
                            post.title,
                            post.content,
                            post.readable_content,
                            post.slug,
                            post.published,
                            post.created,
                            post.updated
                        );
                    });
            }
        });
    }

    onSubmit (form: any) {
        this.submitted = true;

        // Set the slug if it hasn't been set.
        if (this.post.slug === '') {
            this.post.slug = this.slugify(form.title);
        } else {
            this.post.slug = this.slugify(this.post.slug);
        }

        // Prevent submission if there are errors in the title or in the content.
        if (!this.postForm.find('title').valid || !this.postForm.find('content').valid) {
            return;
        }

        // Update a post
        if (this.post.id !== -1) {
            this.dataService.updatePost(this.post, () => {
                this.router.navigateByUrl('/dashboard');
            });
        }
        // Create a post
        else {
            this.dataService.createPost(this.post, () => {
                this.router.navigateByUrl('/dashboard');
            });
        }
    }

    private slugify (text: string) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }
}
