import { provideRouter, RouterConfig } from '@angular/router';
import { DashboardPostListComponent } from '../dashboard-postlist/dashboard-postlist.component';
import { DashboardPostFormComponent } from '../dashboard-postform/dashboard-postform.component';

export const routes: RouterConfig = [
    { path: 'dashboard', component: DashboardPostListComponent },
    { path: 'dashboard/new-post', component: DashboardPostFormComponent },
    { path: 'dashboard/update/:id', component: DashboardPostFormComponent },
];

export const APP_ROUTER_PROVIDERS = [provideRouter(routes)];
