import { Component } from '@angular/core';
import { HTTP_PROVIDERS } from '@angular/http';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { PostListComponent } from '../postlist/postlist.component';
import { DataService } from '../../services/data.service';

@Component({
    directives: [ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS, DataService],
    selector: 'dashboard',
    styleUrls: ['static/app/components/dashboard/dashboard.component.css'],
    templateUrl: 'static/app/components/dashboard/dashboard.component.html'
})
export class DashboardComponent {
    constructor (private dataService: DataService) {}

    ngOnInit () {
        this.dataService.fetchPostsFromDashboardAPI();
    }
}
