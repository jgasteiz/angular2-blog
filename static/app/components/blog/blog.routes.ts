import { provideRouter, RouterConfig } from '@angular/router';
import { PostListComponent } from '../postlist/postlist.component';
import { AboutComponent } from '../about/about.component';
import { DetailComponent } from '../detail/detail.component';

export const routes: RouterConfig = [
    { path: '', component: PostListComponent },
    { path: 'about', component: AboutComponent },
    { path: ':id', component: DetailComponent },
];

export const APP_ROUTER_PROVIDERS = [provideRouter(routes)];
