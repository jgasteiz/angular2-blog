import { Component } from '@angular/core';
import { HTTP_PROVIDERS } from '@angular/http';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { PostListComponent } from '../postlist/postlist.component';
import { DataService } from '../../services/data.service';

@Component({
    directives: [ROUTER_DIRECTIVES, PostListComponent],
    providers: [HTTP_PROVIDERS, DataService],
    selector: 'blog',
    styleUrls: ['static/app/components/blog/blog.component.css'],
    templateUrl: 'static/app/components/blog/blog.component.html'
})
export class BlogComponent {
    constructor (private dataService: DataService) {}

    ngOnInit () {
        this.dataService.fetchPostsFromAPI();
    }
}
