import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Post } from '../../models/post';

@Component({
    directives: [ROUTER_DIRECTIVES],
    inputs: ['post', 'showLink'],
    selector: 'post',
    styleUrls: ['static/app/components/post/post.component.css'],
    templateUrl: 'static/app/components/post/post.component.html'
})
export class PostComponent {
    post: Post;
}
