import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { DataService } from '../../services/data.service';
import { Post } from '../../models/post';

@Component({
    directives: [ROUTER_DIRECTIVES],
    selector: 'dashboard-post-list',
    styleUrls: ['static/app/components/dashboard-postlist/dashboard-postlist.component.css'],
    templateUrl: 'static/app/components/dashboard-postlist/dashboard-postlist.component.html'
})
export class DashboardPostListComponent {
    constructor (private dataService: DataService) {}

    getPosts() {
        return this.dataService.posts;
    }

    deletePost (post: Post) {
        this.dataService.deletePost(post);
        return false;
    }
}
