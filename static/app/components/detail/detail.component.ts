import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from '../../models/post';
import { PostComponent } from '../post/post.component';
import { DataService } from '../../services/data.service';

@Component({
    directives: [PostComponent],
    selector: 'detail',
    templateUrl: 'static/app/components/detail/detail.component.html'
})
export class DetailComponent {
    post: Post;

    constructor (
        private route: ActivatedRoute,
        private router: Router,
        private dataService: DataService
    ) {}

    ngOnInit () {
        this.post = null;

        this.route.params.subscribe((params: any) => {
            this.dataService.fetchPostFromAPI(params['id'])
                .subscribe((post: any) => {
                    this.post = new Post(
                        post.id,
                        post.title,
                        post.content,
                        post.readable_content,
                        post.slug
                    );
                });
        });
    }

}
