import { bootstrap }    from '@angular/platform-browser-dynamic';
import { disableDeprecatedForms, provideForms } from '@angular/forms';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { APP_ROUTER_PROVIDERS } from './components/dashboard/dashboard.routes';

bootstrap(DashboardComponent, [
    APP_ROUTER_PROVIDERS,
    disableDeprecatedForms(),
    provideForms()
]);
