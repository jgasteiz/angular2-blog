import { Injectable } from '@angular/core';
import { Http, Headers, BaseRequestOptions } from '@angular/http';
import { Post } from '../models/post';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
    POSTS_ENDPOINT: string = '/api/blogposts/';
    POSTS_DASHBOARD_ENDPOINT: string = '/dashboard/api/blogposts/';
    posts: Post[];

    constructor (private http: Http) {
        this.posts = [];
    }

    /**
     * Fetch the posts from the api.
     * @return {[type]} [description]
     */
    fetchPostsFromAPI () {
        this.posts = [];
        this.http.get(this.POSTS_ENDPOINT + '?format=json')
            .map(response => response.json())
            .subscribe(posts => {
                posts.forEach(function (post: any) {
                    this.posts.push(new Post(
                        post.id,
                        post.title,
                        post.content,
                        post.readable_content,
                        post.slug
                    ));
                }, this);
            });
    }

    /**
     * Fetch the posts from the api.
     * @return {[type]} [description]
     */
    fetchPostsFromDashboardAPI () {
        this.posts = [];
        this.http.get(this.POSTS_DASHBOARD_ENDPOINT + '?format=json')
            .map(response => response.json())
            .subscribe(posts => {
                posts.forEach(function (post: any) {
                    this.posts.push(new Post(
                        post.id,
                        post.title,
                        post.content,
                        post.readable_content,
                        post.slug,
                        post.published,
                        post.created,
                        post.updated
                    ));
                }, this);
            });
    }

    /**
     * Fetch a post from the api.
     * @param  {string} slug [description]
     * @return {[type]}    [description]
     */
    fetchPostFromAPI (slug: string) {
        return this.http.get(this.POSTS_ENDPOINT + slug + '?format=json')
            .map(response => response.json());
    }

    fetchPostFromDashboardAPI (id: string) {
        return this.http.get(this.POSTS_DASHBOARD_ENDPOINT + id + '?format=json')
            .map(response => response.json());
    }

    /**
     * Create a post with the given properties.
     * @param post
     * @param onComplete
     */
    createPost (post: Post, onComplete: any) {
        return this.http.post('/dashboard/api/blogposts/', {
            title: post.title,
            content: post.content,
            slug: post.slug,
            published: post.published
        }, this._getRequestOptions())
            .map((response: any) => response.json())
            .subscribe(
                (data: any) => {
                    this.posts.push(new Post(
                        data.id,
                        data.title,
                        data.content,
                        data.readable_content,
                        data.slug,
                        data.published,
                        data.created,
                        data.updated
                    ));
                },
                (err:any) => console.error(err),
                onComplete()
            );
    };

    /**
     * Update a given post.
     * @param post
     * @param onComplete
     */
    updatePost (post: Post, onComplete: any) {
        this.http.put('/dashboard/api/blogposts/' + post.id + '/', {
            id: post.id,
            title: post.title,
            content: post.content,
            slug: post.slug,
            published: post.published
        }, this._getRequestOptions())
            .map((response: any) => response.json())
            .subscribe(
                (data: any) => {
                    let postIndex = this.posts
                        .map((post) => post.id )
                        .indexOf(post.id);

                    this.posts[postIndex] = new Post(
                        data.id,
                        data.title,
                        data.content,
                        data.readable_content,
                        data.slug,
                        data.published,
                        data.created,
                        data.updated
                    );
                },
                (err:any) => console.error(err),
                onComplete()
            );
    };

    /**
     * Delete a given post.
     * @param post
     */
    deletePost (post: Post) {
        if (!confirm('Are you sure you want to delete the post `' + post.title + '`?')) {
            return false;
        }
        this.http.delete('/dashboard/api/blogposts/' + post.id + '/', this._getRequestOptions())
            .subscribe(
                (data: any) => {
                    let postIndex = this.posts
                        .map((post) => post.id )
                        .indexOf(post.id);

                    this.posts.splice(postIndex, 1);
                }
            );
    };

    /**
     * Return the request options for PUT, POST and DELETE requests.
     * @returns {CustomRequestOptions}
     * @private
     */
    _getRequestOptions () {
        let headers = new Headers({'Content-Type': 'application/json'});
        return new CustomRequestOptions({headers: headers});
    };
}

class CustomRequestOptions extends BaseRequestOptions {
    constructor(options:Object) {
        super();
        if (options) {
            this.headers = options['headers'];
        }
        this.headers.append('X-CSRFToken', this.getCookie('csrftoken'));
    }

    getCookie(name: string) {
        let value = "; " + document.cookie;
        let parts = value.split("; " + name + "=");
        if (parts.length === 2) {
            return parts.pop().split(";").shift();
        }
    }
}
