export class Post {
    id: number;
    title: string;
    content: string;
    readable_content: string;
    slug: string;

    published: boolean;
    created: string;
    updated: string;

    updateUrl: string;

    constructor(
        id: number,
        title: string,
        content: string,
        readable_content: string,
        slug: string,
        published?: boolean,
        created?: string,
        updated?: string
    ) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.readable_content = readable_content;
        this.slug = slug;

        this.published = published || false;
        this.created = created || '';
        this.updated = updated || '';

        this.updateUrl = '/dashboard/update/' + id;
    }
}
