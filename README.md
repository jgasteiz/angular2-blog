# Angular 2 Blog

Simple blog built using Django and Angular 2.

## Postgres db setup

- `psql`
- `CREATE DATABASE angular2blog;`
- `CREATE USER angular2blog WITH PASSWORD 1234;`
- `GRANT ALL PRIVILEGES ON DATABASE angular2blog TO angular2blog;`

## Project setup

- `python3 -m venv env`
- `source ./env/bin/activate`
- `pip install -r requirements.txt`
- `./manage.py migrate --settings=angular2blog.local_settings`
- `./manage.py createsuperuser --settings=angular2blog.local_settings`
- `npm install`
- `npm install typings`

## Run it locally

- `./scripts/serve`
- `npm start`
