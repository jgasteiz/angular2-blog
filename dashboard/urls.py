from django.conf.urls import url, include
from rest_framework import routers
from dashboard import views

router = routers.DefaultRouter()
router.register(r'blogposts', views.BlogPostViewSet)


urlpatterns = [
    url(r'^login/', views.login_view, name='login'),
    url(r'^api/', include(router.urls)),
    url(r'^update/(?P<id>\w+)/$', views.dashboard, name='index'),
    url(r'^new-post/$', views.dashboard, name='index'),
    url(r'^$', views.dashboard, name='index'),
]
