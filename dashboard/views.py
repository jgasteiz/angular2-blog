from django.contrib.auth import authenticate, login, decorators
from django.core.urlresolvers import reverse_lazy, reverse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import FormView, TemplateView
from rest_framework import viewsets, permissions

from blog.models import BlogPost, DashboardBlogPostSerializer
from dashboard.forms import LoginForm


class LoginRequiredMixin(object):
    """
    Ensures that user must be authenticated in order to access view.
    """
    @method_decorator(decorators.login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = 'dashboard/index.html'

dashboard = DashboardView.as_view()


class BlogPostViewSet(viewsets.ModelViewSet):
    queryset = BlogPost.objects.all()
    serializer_class = DashboardBlogPostSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'dashboard/login.html'
    success_url = reverse_lazy('dashboard:index')
    login_url = reverse_lazy('dashboard:login')

    def form_valid(self, form):
        cd = form.cleaned_data
        user = authenticate(username=cd.get('user'), password=cd.get('password'))
        if user and user.is_authenticated:
            login(self.request, user)
            return redirect(self.get_success_url())
        return redirect(self.login_url)


login_view = LoginView.as_view()
