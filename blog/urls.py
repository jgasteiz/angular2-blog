from django.conf.urls import url, include
from rest_framework import routers
from blog import views

router = routers.DefaultRouter()
router.register(r'blogposts', views.BlogPostViewSet)


urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^', views.index, name='index'),
]
