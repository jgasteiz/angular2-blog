import markdown
from django.db import models
from rest_framework import routers, serializers, viewsets


class BlogPost(models.Model):
    title = models.CharField(max_length=512)
    content = models.TextField()
    slug = models.CharField(max_length=512)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    published = models.BooleanField(default=False)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.title


class BlogPostSerializer(serializers.ModelSerializer):
    readable_content = serializers.SerializerMethodField()

    class Meta:
        model = BlogPost
        lookup_field = 'slug'
        fields = ('id', 'title', 'content', 'readable_content', 'slug')

    def get_readable_content(self, obj):
        return markdown.markdown(obj.content)


class DashboardBlogPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlogPost
        fields = ('id', 'title', 'content', 'slug', 'created', 'updated', 'published')
