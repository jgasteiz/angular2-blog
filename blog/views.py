from django.views.generic import TemplateView
from rest_framework import viewsets, permissions

from blog.models import BlogPost, BlogPostSerializer


class IndexView(TemplateView):
    template_name = 'blog/index.html'

index = IndexView.as_view()


class BlogPostViewSet(viewsets.ModelViewSet):
    queryset = BlogPost.objects.filter(published=True)
    lookup_field = 'slug'
    serializer_class = BlogPostSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
